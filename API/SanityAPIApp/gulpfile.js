﻿var gulp = require('gulp'),
    nodemon = require('gulp-nodemon');
var concat = require('gulp-concat');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var s3 = require("gulp-s3");
var fs = require("fs");
var tsc = require("gulp-typescript-compiler");
//var awsCredentials = JSON.parse(fs.readFileSync('./aws.json'));

var input = {
    'sass': 'source/scss/**/*.scss',
    'javascript': 'public/**/*.js',
    'vendorjs': 'public/assets/javascript/vendor/**/*.js'
},

    output = {
    'stylesheets': 'public/assets/stylesheets',
    'javascript': 'build/javascript'
};

//gulp.task('default', function () {
//    nodemon({
//        script: 'server.js',
//        ext: 'js',
//        env: {
//            PORT: 5000
//        }
//    }).on('restart', function () {
//        console.log('Restarting on gulp');
//    });
//});

gulp.task('build-js', function () {
    return gulp.src([input.javascript, '!public/lib/**'])
        .pipe(sourcemaps.init())
          .pipe(concat('bundle.js'))
          //only uglify if gulp is ran with '--type production'
          //.pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
          //.pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(output.javascript));
});


gulp.task('upload', function () {
    return gulp.src('build/javascript/bundle.js')
    .pipe(s3({
        "key": "AKIAJUQKO2FVOUSOHSTQ",
        "secret": "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",
        "bucket": "trongtest1",
        "region": "ap-southeast-1"
    },{
            uploadPath: "",
            headers: {
        'x-amz-acl': 'public-read'
         }
       }));
});

//gulp.task('default', ['build-js']);

//gulp.task('deploy', ['build-js', 'upload']);


gulp.task("default", ["compile", "watch", "nodemon"]);


gulp.task("watch", function () {
    return gulp.watch("src/**/*.*", ["compile"]);
});

gulp.task("compile", function () {
    return gulp
    .src("src/**/*.ts")
    .pipe(tsc({
        module: "commonjs",
        target: "ES5",
        sourcemap: false,
        logErrors: true
    }))
    .pipe(gulp.dest("lib"));
});

gulp.task("nodemon", function () {
    nodemon({ script: "lib/server.js" });
});
