﻿'use strict';

angular.module('emails').controller('EmailsController', ['$scope',
    '$routeParams', '$location', '$http', 'EmailsService',
    function ($scope, $routeParams, $location, $http, EmailsSvc) {
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 5;

        $scope.create = function () {
            var email = new EmailsSvc({
                subject: this.subject,
                content: this.content,
                recipients: this.recipients
            });
            email.$save(function (response) {
                $location.path('emails/' + response._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        
        $scope.getEmails = function () {
            $http.get('/api/emails', { params: { page: $scope.currentPage, } }).success(function (response) {
                $scope.emails = response.emails;
                $scope.totalItems = response.totalItems;
            }).error(function (response) {
                $scope.error = response.message;
            });
        };

        $scope.find = function () {
            $scope.emails = EmailsSvc.query();
        };
        
        $scope.findOne = function () {
            $scope.email = EmailsSvc.get({
                emailId: $routeParams.emailId
            });
        };
        
        $scope.update = function () {
            $scope.email.$update(function () {
                $location.path('emails/' + $scope.email._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        
        $scope.delete = function (email) {
            if (email) {
                EmailsSvc.remove({ _id: email._id }, (function() {
                    for (var i in $scope.emails) {
                        if ($scope.emails[i] === email) {
                            $scope.emails.splice(i, 1);
                            break;
                        }
                    }
                }));
            } else {
                EmailsSvc.remove({ _id: email._id }, (function () {
                    $location.path('emails');
                }));
            }
        };
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            $scope.getEmails();
        };

        $scope.sendEmail = function(email) {
            $http.post('/api/emails/send', {email:email}).success(function (response) {

            }).error(function (response) {
                $scope.error = response.message;
            });
        }

    }
]);