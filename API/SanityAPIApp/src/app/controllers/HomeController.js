var HomeController = (function () {
    function HomeController() {
    }
    HomeController.prototype.index = function (req, res) {
        res.render('index.html', {
            CDN: 'build/javascript/'
        });
    };
    return HomeController;
})();
module.exports = HomeController;
//# sourceMappingURL=HomeController.js.map