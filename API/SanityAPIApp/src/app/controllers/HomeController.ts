﻿import express = require("express");
import IHomeController = require("./IHomeController");

class HomeController implements IHomeController {
    
    index(req: express.Request, res: express.Response): void {
        res.render('index.html', {
            CDN: 'build/javascript/'
        });
    }
}

export = HomeController;