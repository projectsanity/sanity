﻿import express = require("express");

interface IHomeController {
    index : (req: express.Request, res: express.Response) => void;
}

export = IHomeController;