///<reference path="../../../../typings/express/express.d.ts"/>
/// <reference path="../../../../typings/inversify/inversify.d.ts" />
var EmailController = (function () {
    //private self;
    function EmailController(IEmailService) {
        this._emailService = IEmailService;
    }
    EmailController.prototype.create = function (req, res) {
        try {
            var email = req.body;
            this._emailService.create(email, function (error, result) {
                if (error)
                    res.send({ "error": "error" });
                else
                    res.json(result);
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });
        }
    };
    EmailController.prototype.update = function (req, res) {
        try {
            var email = req.body;
            var _id = req.params._id;
            this._emailService.update(_id, email, function (error, result) {
                if (error)
                    res.send({ "error": "error" });
                else
                    res.json(email);
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });
        }
    };
    EmailController.prototype.delete = function (req, res) {
        try {
            var _id = req.params._id;
            this._emailService.delete(_id, function (error, result) {
                if (error)
                    res.send({ "error": "error" });
                else
                    res.send({ "success": "success" });
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });
        }
    };
    EmailController.prototype.retrieve = function (req, res) {
        try {
            this._emailService.retrieve(req.query.page, function (error, count, result) {
                if (error)
                    res.send({ "error": "error" });
                else {
                    res.json({
                        totalItems: count,
                        emails: result
                    });
                }
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });
        }
    };
    EmailController.prototype.findById = function (req, res) {
        try {
            var _id = req.params._id;
            this._emailService.findById(_id, function (error, result) {
                if (error)
                    res.send({ "error": "error" });
                else
                    res.send(result);
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });
        }
    };
    return EmailController;
})();
module.exports = EmailController;
//# sourceMappingURL=EmailController.js.map