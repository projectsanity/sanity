﻿///<reference path="../../../../typings/express/express.d.ts"/>

import express = require("express");

interface IEmailController {
    create: (req: express.Request, res: express.Response) => void;
    update: (req: express.Request, res: express.Response) => void;
    delete: (req: express.Request, res: express.Response) => void;
    retrieve: (req: express.Request, res: express.Response) => void;
    findById: (req: express.Request, res: express.Response) => void;
}

export = IEmailController;