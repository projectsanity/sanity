﻿import mongoose = require("mongoose");

 interface EmailModel extends mongoose.Document {
    created: Date;
    subject: string;
    content: string;
    recipients: string;
    fullDetail: string;
}

export =  EmailModel;