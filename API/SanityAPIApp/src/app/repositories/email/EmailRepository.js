var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var EmailSchema = require("../../schemas/email/EmailSchema");
var RepositoryBase = require("../RepositoryBase");
var EmailRepository = (function (_super) {
    __extends(EmailRepository, _super);
    function EmailRepository() {
        _super.call(this, EmailSchema);
    }
    return EmailRepository;
})(RepositoryBase);
Object.seal(EmailRepository);
module.exports = EmailRepository;
//# sourceMappingURL=EmailRepository.js.map