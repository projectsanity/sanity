﻿import EmailModel = require("../../models/email/EmailModel");
import EmailSchema = require("../../schemas/email/EmailSchema");
import RepositoryBase = require("../RepositoryBase");
import IEmailRepository = require("./IEmailRepository");

class EmailRepository extends RepositoryBase<EmailModel> implements IEmailRepository {
    constructor() {
        super(EmailSchema);
    }
} 

Object.seal(EmailRepository);
export = EmailRepository;