/// <reference path="../../config/IocConfiguration.ts" />
var express = require("express");
var inversify = require("inversify");
var router = express.Router();
var EmailRoute = (function () {
    function EmailRoute(IEmailController) {
        //var emailService = new EmailService();
        //var emailService = IocConfiguration.resolve<IEmailService>("IEmailService");
        //this._emailController = new EmailController(emailService);
        this._emailController = IEmailController;
    }
    Object.defineProperty(EmailRoute.prototype, "routes", {
        get: function () {
            var controller = this._emailController;
            router.get("/api/emails", controller.retrieve.bind(controller));
            router.post("/api/emails", controller.create.bind(controller));
            router.put("/api/emails/:_id", controller.update.bind(controller));
            router.get("/api/emails/:_id", controller.findById.bind(controller));
            router.delete("/api/emails/:_id", controller.delete.bind(controller));
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return EmailRoute;
})();
Object.seal(EmailRoute);
module.exports = EmailRoute;
//# sourceMappingURL=EmailRoute.js.map