var express = require("express");
var router = express.Router();
var HomeRoute = (function () {
    function HomeRoute(IHomeController) {
        this._homeController = IHomeController;
    }
    Object.defineProperty(HomeRoute.prototype, "routes", {
        get: function () {
            var controller = this._homeController;
            router.get("/", controller.index.bind(controller));
            return router;
        },
        enumerable: true,
        configurable: true
    });
    return HomeRoute;
})();
Object.seal(HomeRoute);
module.exports = HomeRoute;
//# sourceMappingURL=HomeRoute.js.map