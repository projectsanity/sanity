﻿import DataAccess = require("../../repositories/DataContext");
import EmailModel = require("../../models/email/EmailModel");

var mongoose = DataAccess.mongooseInstance;
var mongooseConnection = DataAccess.mongooseConnection;

class EmailSchema {
    static get schema() {
        var schema = new mongoose.Schema({
            created: {
                type: Date,
                default: Date.now
            },
            subject: {
                type: String,
                trim: true,
                required: 'Subject cannot be blank'
            },
            content: {
                type: String,
                default: ''
            },
            recipients: {
                type: String
            }
        });

        schema.virtual('fullDetail').get(function () {
                return this.subject + ' ' + this.content;
            });

        schema.set('toJSON', { getters: true, virtuals: true });
        schema.set('toObject', { getters: true, virtuals: true });
        return schema;
    }
}

var schema = mongooseConnection.model<EmailModel>('Email', EmailSchema.schema);
export = schema;