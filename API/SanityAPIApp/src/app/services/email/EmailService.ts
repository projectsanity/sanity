﻿import IEmailService = require("IEmailService");
import EmailRepository = require("../../repositories/email/EmailRepository");
import EmailModel = require("../../models/email/EmailModel");
import IEmailRepository = require("../../repositories/email/IEmailRepository");

class EmailService implements IEmailService {
    private _emailRepository: IEmailRepository;

    constructor(IEmailRepository: IEmailRepository) {
        this._emailRepository = IEmailRepository;
    }

    create(item: EmailModel, callback: (error: any, result: any) => void) {
        this._emailRepository.create(item, callback);
    }

    retrieve(page: number, callback: (error: any, count:number, result: any) => void) {
        this._emailRepository.retrieve(page, callback);
    }

    update(_id: string, item: EmailModel, callback: (error: any, result: any) => void) {
        this._emailRepository.findById(_id, (err, res) => {
            if (err) callback(err, res);
            else
                this._emailRepository.update(res._id, item, callback);

        });
    }

    delete(_id: string, callback: (error: any, result: any) => void) {
        this._emailRepository.delete(_id, callback);
    }

    findById(_id: string, callback: (error: any, result: EmailModel) => void) {
        this._emailRepository.findById(_id, callback);
    }
}

Object.seal(EmailService);
export = EmailService;