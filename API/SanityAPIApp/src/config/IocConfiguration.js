/// <reference path="../../typings/inversify/inversify.d.ts" />
/// <reference path="../app/repositories/email/EmailRepository.ts" />
var inversify = require("inversify");
var EmailService = require("../app/services/email/EmailService");
var EmailController = require("../app/controllers/email/EmailController");
var EmailRoute = require("../app/routes/EmailRoute");
var EmailRepository = require("../app/repositories/email/EmailRepository");
var HomeRoute = require("../app/routes/HomeRoute");
var HomeController = require("../app/controllers/HomeController");
var IocConfiguration = (function () {
    function IocConfiguration() {
    }
    IocConfiguration.setup = function () {
        var kernel = new inversify.Kernel();
        kernel.bind(new inversify.TypeBinding("IHomeRoute", HomeRoute, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IHomeController", HomeController, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IEmailRoute", EmailRoute, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IEmailController", EmailController, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IEmailService", EmailService, inversify.TypeBindingScopeEnum.Transient));
        kernel.bind(new inversify.TypeBinding("IEmailRepository", EmailRepository, inversify.TypeBindingScopeEnum.Transient));
        console.log("IOC register OK");
        return kernel;
    };
    return IocConfiguration;
})();
Object.seal(IocConfiguration);
module.exports = IocConfiguration;
//# sourceMappingURL=IocConfiguration.js.map