﻿var config = require('../config/env/development'),
mongoose = require('mongoose');

module.exports = function () {
    var db = mongoose.connect(config.db);

    require('../app/models/email/email.server.model');
    
    require('../app/models/email/sharding.server.model');

    //require('../app/models/article.server.model');

    return db;
};