﻿///<reference path="../typings/express/express.d.ts"/>

import express = require("express");
import Configuration = require("./config/Configuration");
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var app = express();
var port = parseInt(process.env.PORT, 10) || 5000;
app.set("port", port);

app.use(Configuration.setup);

app.listen(port, () => {
    console.log("Node app is running at localhost:" + port);
});


//var express = require('./config/express');
//var mongoose = require('./config/mongoose');

//var db = mongoose();
//var app = express();
//app.listen(process.env.PORT || 5000);
//module.exports = app;

//console.log('Server running at http://localhost:5000/');